### Start angular service

cd angularProject ;

ng serve -o


### Start node API

cd api ;

npm start


### Start mongod service

sudo mongo --port 27018
